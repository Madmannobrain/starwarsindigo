/obj/item/weapon/gun/energy/laser
	name = "laser gun"
	desc = "a basic weapon designed kill with concentrated energy bolts"
	icon_state = "laser"
	item_state = "laser"
	fire_sound = 'sound/weapons/Laser.ogg'
	w_class = 3.0
	m_amt = 2000
	origin_tech = "combat=3;magnets=2"
	projectile_type = "/obj/item/projectile/beam"

/obj/item/weapon/gun/energy/laser/practice
	name = "practice laser gun"
	desc = "A modified version of the basic laser gun, this one fires less concentrated energy bolts designed for target practice."
	projectile_type = "/obj/item/projectile/beam/practice"
	clumsy_check = 0

obj/item/weapon/gun/energy/laser/e11
	name ="E11 carbine"
	icon_state = "e11"
	desc = "A powerful, light and compact weapon, the E-11 was used widely through the galaxy."
	projectile_type = "/obj/item/projectile/beam/swred"
	charge_cost = 35
	fire_sound = 'sound/weapons/laser.ogg'
	w_class = 3.5

obj/item/weapon/gun/energy/laser/bowcaster
	name ="wookie bowcaster"
	icon_state = "bowcaster"
	desc = "A traditional weapon used by wookies"
	projectile_type = "/obj/item/projectile/beam/swgreen"
	charge_cost = 35
	fire_sound = 'sound/weapons/laser.ogg'
	w_class = 3.5

obj/item/weapon/gun/energy/laser/a280
	name ="A280 laser rifle"
	icon_state = "a280"
	desc = "The A280 was a blaster rifle produced by BlasTech Industries, and was considered to be one of the best armor-piercing blasters produced during the Galactic Civil War. "
	projectile_type = "/obj/item/projectile/beam/swred"
	charge_cost = 35
	fire_sound = 'sound/weapons/laser.ogg'
	w_class = 4.0

obj/item/weapon/gun/energy/laser/ee3
	name ="EE-3 carbine"
	icon_state = "ee3"
	desc = "A compact, fast-firing weapon which used by bounty hunters"
	projectile_type = "/obj/item/projectile/beam/swred"
	charge_cost = 20
	fire_sound = 'sound/weapons/laser.ogg'
	w_class = 3.5

obj/item/weapon/gun/energy/laser/dh17
	name ="DH-17 pistol"
	icon_state = "dh17"
	desc = "A compact, fast-firing weapon which used by imperial officers"
	projectile_type = "/obj/item/projectile/beam/swred"
	charge_cost = 50
	fire_sound = 'sound/weapons/laser.ogg'

obj/item/weapon/gun/energy/laser/retro
	name ="retro laser"
	icon_state = "retro"


/obj/item/weapon/gun/energy/laser/captain
	icon_state = "caplaser"
	desc = "This is an antique laser gun. All craftsmanship is of the highest quality. It is decorated with assistant leather and chrome. The object menaces with spikes of energy. On the item is an image of Space Station 13. The station is exploding."
	force = 10
	origin_tech = null
	var/charge_tick = 0


	New()
		..()
		processing_objects.Add(src)


	Del()
		processing_objects.Remove(src)
		..()


	process()
		charge_tick++
		if(charge_tick < 4) return 0
		charge_tick = 0
		if(!power_supply) return 0
		power_supply.give(100)
		update_icon()
		return 1



/obj/item/weapon/gun/energy/laser/cyborg/process_chambered()
	if(in_chamber)
		return 1
	if(isrobot(src.loc))
		var/mob/living/silicon/robot/R = src.loc
		if(R && R.cell)
			R.cell.use(100)
			in_chamber = new/obj/item/projectile/beam(src)
			return 1
	return 0


/obj/item/weapon/gun/energy/laser/captain/dl44
	icon_state = "dl44"
	name = "DL-44"
	desc = "A blaster which favored by the outlaws of the all galaxy."
	projectile_type = "/obj/item/projectile/beam/swred/dl44"
	fire_sound = 'sound/weapons/laser3.ogg'

/obj/item/weapon/gun/energy/lasercannon
	name = "laser cannon"
	desc = "With the L.A.S.E.R. cannon, the lasing medium is enclosed in a tube lined with uranium-235 and subjected to high neutron flux in a nuclear reactor core. This incredible technology may help YOU achieve high excitation rates with small laser volumes!"
	icon_state = "lasercannon"
	fire_sound = 'sound/weapons/lasercannonfire.ogg'
	origin_tech = "combat=4;materials=3;powerstorage=3"
	projectile_type = "/obj/item/projectile/beam/heavylaser"

	fire_delay = 20

	isHandgun()
		return 0

/obj/item/weapon/gun/energy/lasercannon/cyborg/process_chambered()
	if(in_chamber)
		return 1
	if(isrobot(src.loc))
		var/mob/living/silicon/robot/R = src.loc
		if(R && R.cell)
			R.cell.use(250)
			in_chamber = new/obj/item/projectile/beam(src)
			return 1
	return 0

/obj/item/weapon/gun/energy/xray
	name = "xray laser gun"
	desc = "A high-power laser gun capable of expelling concentrated xray blasts."
	icon_state = "xray"
	fire_sound = 'sound/weapons/laser3.ogg'
	origin_tech = "combat=5;materials=3;magnets=2;syndicate=2"
	projectile_type = "/obj/item/projectile/beam/xray"
	charge_cost = 50


////////Laser Tag////////////////////

/obj/item/weapon/gun/energy/laser/bluetag
	name = "laser tag gun"
	icon_state = "bluetag"
	desc = "Standard issue weapon of the Imperial Guard"
	projectile_type = "/obj/item/projectile/beam/lastertag/blue"
	origin_tech = "combat=1;magnets=2"
	clumsy_check = 0
	var/charge_tick = 0

	special_check(var/mob/living/carbon/human/M)
		if(ishuman(M))
			if(istype(M.wear_suit, /obj/item/clothing/suit/bluetag))
				return 1
			M << "\red You need to be wearing your laser tag vest!"
		return 0

	New()
		..()
		processing_objects.Add(src)


	Del()
		processing_objects.Remove(src)
		..()


	process()
		charge_tick++
		if(charge_tick < 4) return 0
		charge_tick = 0
		if(!power_supply) return 0
		power_supply.give(100)
		update_icon()
		return 1



/obj/item/weapon/gun/energy/laser/redtag
	name = "laser tag gun"
	icon_state = "redtag"
	desc = "Standard issue weapon of the Imperial Guard"
	projectile_type = "/obj/item/projectile/beam/lastertag/red"
	origin_tech = "combat=1;magnets=2"
	clumsy_check = 0
	var/charge_tick = 0

	special_check(var/mob/living/carbon/human/M)
		if(ishuman(M))
			if(istype(M.wear_suit, /obj/item/clothing/suit/redtag))
				return 1
			M << "\red You need to be wearing your laser tag vest!"
		return 0

	New()
		..()
		processing_objects.Add(src)


	Del()
		processing_objects.Remove(src)
		..()


	process()
		charge_tick++
		if(charge_tick < 4) return 0
		charge_tick = 0
		if(!power_supply) return 0
		power_supply.give(100)
		update_icon()
		return 1