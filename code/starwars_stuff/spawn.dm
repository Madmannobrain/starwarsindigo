client/proc/spawn_by_ckey()
	set category = "Vyetnam"
	set name = "Spawn by ckey"

	var/player_ckey = input(usr, "Enter player ckey", "Spawn by ckey")

	if(!player_ckey)
		return

	var/client/player = null
	for(var/client/C)
		if(player_ckey == C.key)
			player = C
			break

	if(!player)
		usr << "\red User not found."
		return

	var/list/possible_equipment = list("Marine", "Sergeant", "Officer", "Stormtrooper", "Jedi", "Sith", "Fett", "Silentium")

	var/equipment = input(usr, "Select equipment", "Equipment") in possible_equipment

	if(!equipment || !player)
		return

	var/mob/new_player/newPlayer = new(usr.loc)
	newPlayer.client = player
	var/mob/living/carbon/human/M = newPlayer.create_character()
	M.loc = usr.loc

	switch(equipment)
		if("Marine")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/rebel(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/jackboots(M), slot_shoes)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/rebel_helm(M), slot_head)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/a280(M), slot_belt)
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Rebel Marine)"
			W.access = get_all_accesses()
			W.assignment = "Rebel"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

		if("Sergeant")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/rebel_officer(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/rebel_helm(M), slot_head)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/captain/dl44(M), slot_belt)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/jackboots(M), slot_shoes)
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Rebel Sergeant)"
			W.access = get_all_accesses()
			W.assignment = "Rebel Sergeant"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

		if("Officer")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/imperial_officer(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/dh17(M), slot_belt)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/jackboots(M), slot_shoes)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/soft(M), slot_head)
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Empire Officer)"
			W.access = get_all_accesses()
			W.assignment = "Officer"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

		if("Stormtrooper")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/stormtrooper(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/e11(M), slot_belt)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/jackboots(M), slot_shoes)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/helmet/stormtrooper_helmet(M), slot_head)
			M.equip_to_slot_or_del(new /obj/item/clothing/suit/armor/stormtrooper(M), slot_wear_suit)
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Stormtrooper)"
			W.access = get_all_accesses()
			W.assignment = "Stormtrooper"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

		if("Jedi")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/jedi(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/weapon/melee/energy/sword/green(M), slot_r_store)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/brown(M), slot_shoes)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/jedi_hood(M), slot_head)
			M.equip_to_slot_or_del(new /obj/item/clothing/suit/armor/jedi(M), slot_wear_suit)
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Jedi Knight)"
			W.access = get_all_accesses()
			W.assignment = "Jedi"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

		if("Sith")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/sith(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/weapon/melee/energy/sword/red(M), slot_r_store)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/brown(M), slot_shoes)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/sith_hood(M), slot_head)
			M.equip_to_slot_or_del(new /obj/item/clothing/suit/armor/sith(M), slot_wear_suit)
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Sith)"
			W.access = get_all_accesses()
			W.assignment = "Sith"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

		if("Fett")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/syndicate(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/ee3(M), slot_r_store)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/swat(M), slot_shoes)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/helmet/space/fett(M), slot_head)
			M.equip_to_slot_or_del(new /obj/item/clothing/suit/space/fett(M), slot_wear_suit)
			M.equip_to_slot_or_del(new /obj/item/clothing/glasses/thermal(M), slot_glasses)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/ee3(M), slot_l_hand)
			M.equip_to_slot_or_del(new /obj/item/weapon/grenade/smokebomb(M), slot_r_store)
			M.equip_to_slot_or_del(new /obj/item/weapon/grenade/empgrenade(M), slot_l_store)
			var /obj/item/weapon/tank/jetpack/J = new /obj/item/weapon/tank/jetpack/fett(M)
			M.equip_to_slot_or_del(J, slot_back)
			J.toggle()
			M.equip_to_slot_or_del(new /obj/item/clothing/mask/breath(M), slot_wear_mask)
			J.Topic(null, list("stat" = 1))
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Bounty Hunter)"
			W.access = get_all_accesses()
			W.assignment = "Bounty Hunter"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

		if("Silentium")
			M.equip_to_slot_or_del(new /obj/item/clothing/under/syndicate(M), slot_w_uniform)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/captain/dl44(M), slot_belt)
			M.equip_to_slot_or_del(new /obj/item/clothing/shoes/swat(M), slot_shoes)
			M.equip_to_slot_or_del(new /obj/item/clothing/head/helmet/space/fett/silentium(M), slot_head)
			M.equip_to_slot_or_del(new /obj/item/clothing/suit/space/fett/silentium(M), slot_wear_suit)
			M.equip_to_slot_or_del(new /obj/item/clothing/glasses/thermal(M), slot_glasses)
			M.equip_to_slot_or_del(new /obj/item/weapon/gun/energy/laser/captain/dl44(M), slot_l_hand)
			M.equip_to_slot_or_del(new /obj/item/weapon/grenade/smokebomb(M), slot_r_store)
			M.equip_to_slot_or_del(new /obj/item/weapon/grenade/empgrenade(M), slot_l_store)
			var /obj/item/weapon/tank/jetpack/J = new /obj/item/weapon/tank/jetpack/fett(M)
			M.equip_to_slot_or_del(J, slot_back)
			J.toggle()
			M.equip_to_slot_or_del(new /obj/item/clothing/mask/breath(M), slot_wear_mask)
			J.Topic(null, list("stat" = 1))
			var/obj/item/weapon/card/id/W = new(M)
			W.name = "[M.real_name]'s ID Card (Bounty Hunter)"
			W.access = get_all_accesses()
			W.assignment = "Bounty Hunter"
			W.registered_name = M.real_name
			M.equip_to_slot_or_del(W, slot_wear_id)

